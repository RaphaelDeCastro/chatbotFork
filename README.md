# ChatBot


## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name

ChatBoX

## Description

*WIP*

**ChatBoX** is a ChatBot, this is a computer program designed to simulate conversation with a real user. But this one is specially desgined to talk about SpaceX.

## Visuals

*WIP (gif || img)*

## Installation

*WIP (Installation guide)*

## Usage

**First usage**
- User login on the website and ask ChatBox "When SpaceX was created ?"
- Chatbox answer "SpaceX was created the 14 March 2002 !"

## Support

*WIP (add a mail or anything else that can help people to learn about how it works)* 

## Roadmap

*WIP (roadmap in progress, need to list here the roadmap content)*

## Authors and acknowledgment

Denis Demoineret - Backend
Ambre Nguyen - Frontend
Quentin Michel - Data Scientist
Assia Oumri - Product Owner
Raphaël De Castro - UX/UI & Intégrateur HTML/CSS